[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![Version](https://img.shields.io/badge/Version-1.0-<COLOR>.svg)](https://shields.io/)
[![GitHub license](https://img.shields.io/github/license/Naereen/StrapDown.js.svg)](https://github.com/Naereen/StrapDown.js/blob/master/LICENSE)
[![GitHub release](https://img.shields.io/github/release/Naereen/StrapDown.js.svg)](https://github.com/19wh1a0505/mail-scheduler/releases/)
![GitHub contributors](https://img.shields.io/github/contributors/19wh1a0505/mail-scheduler.svg)

# Mail scheduler

MERN Stands For MongoDB, ExpressJs, ReactJs and Node.

This project is created to Schedule mail for any time using MERN Stack.

The structure of this project is inspired from [LARAVEL](https://laravel.com) Framework - Most Popular php framework

# [Documentation](https://mail-scheduler.github.io/docs/index.html)


## Contributions

All Contributions and suggestions are welcome. Please read [Contribution Guidelines](https://github.com/19wh1a0505/mail-scheduler/blob/master/CONTRIBUTING.md)

## Issues
Please use GitHub Issue Tracker For Issues.

## License

The project is an open-source software licensed under the [MIT license](https://github.com/19wh1a0505/mail-scheduler/blob/master/LICENSE).
